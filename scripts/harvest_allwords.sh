#/bin/bash


  PADBASEURL="http://pipelines.constantvzw.org/etherdump"
  ALLPADNAMES="http://pipelines.constantvzw.org/etherdump/allpads.txt"
  XFORMAT="txt"

  TMP="/tmp/pppp"
  DUMPHERE="${TMP}_allpads-with-name.dump"
  HTMLOUT="$1"
  if [ -f $HTMLOUT ]; then
       echo "$HTMLOUT already exists"; exit;
  fi
  echo "<html><meta charset=\"UTF-8\"/><body>\
        <p style=\"text-align:justify;\">" > $HTMLOUT

# --------------------------------------------------------------------------- #
#  D U M P   A L L   P A D S   I N T O   O N E (IF NECESSARY)
# --------------------------------------------------------------------------- #
  if [ -f $DUMPHERE ]; then
      #echo "DUMP does exist ($DUMPHERE)"
       read -p "UPDATE DUMP FROM REMOTE? [y/n] " ANSWER
       if    [ X$ANSWER == Xy ];
          then rm $DUMPHERE 
        elif [ X$ANSWER != Xn ];
          then  echo "You should answer with y or n!"; exit;
       fi
  fi;  
  if [ ! -f $DUMPHERE ]; then
       for PAD in `wget --no-check-certificate -q -O - $ALLPADNAMES | # 
                   egrep -v "graph_meandering|references.bib" | # IGNORE THIS
                   sed "s,^,$PADBASEURL/," | sed "s,$,.$XFORMAT," `
        do
            echo $PAD
            PADNAME=`basename $PAD`
            wget --no-check-certificate -q -O - $PAD | #
            sed "s,^,${PADNAME}:," >> $DUMPHERE
       done
  fi

# --------------------------------------------------------------------------- #
#  M A K E   W O R D L I S T 
# --------------------------------------------------------------------------- #

  cat $DUMPHERE              | # 
  cut -d ":" -f 2-           | #
  sed 's/[^a-zA-Z]/\n/g'     | #
  sed -n '/^.\{8\}/p'        | #
  sort -fu > ${TMP}_words.list

# --------------------------------------------------------------------------- #
#  L O O K   F O R   K E Y W O R D 
# --------------------------------------------------------------------------- #

  for WORD in `cat ${TMP}_words.list`
   do
      echo "<span style=\"white-space:nowrap;\">"   >> $HTMLOUT
      echo "◍ "                                     >> $HTMLOUT
      echo "$WORD"                                  >> $HTMLOUT
      for MATCH in `grep -i "$WORD" $DUMPHERE | #
                    cut -d ":" -f 1 | sort -u`
       do
          MATCHURL=`echo "$PADBASEURL/$MATCH" | sed 's/\.txt$/.html/'`
          echo "</span>"                            >> $HTMLOUT
          echo "<a href=\"$MATCHURL\">→ </a>"       >> $HTMLOUT
      done
      echo "&nbsp;&nbsp;&nbsp;"                     >> $HTMLOUT
  done

# --------------------------------------------------------------------------- #
#  F I N I S H
# --------------------------------------------------------------------------- #

  echo "</p></body></html>" >> $HTMLOUT





exit 0;
