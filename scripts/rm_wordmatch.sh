#/bin/bash


  PADBASEURL="http://pipelines.constantvzw.org/etherdump"
  ALLPADNAMES="http://pipelines.constantvzw.org/etherdump/allpads.txt"
  XFORMAT="txt"

  TMP="/tmp/pppp"
  DUMPHERE="${TMP}_allpads-with-name.dump"
   HTMLDIR="html"

  KEYWORD="$*"

# --------------------------------------------------------------------------- #
#  M A K E  S U R E  T H E R E  I S  S O M E T H I N G   T O   L O O K  F O R
# --------------------------------------------------------------------------- #
  if [ `echo $KEYWORD        | #
        sed 's/[^a-zA-Z]//g' | #
        wc -c` -lt 2 ]
   then
       clear
       echo "What do we want to forget?"; echo
       read -p "" KEYWORD
  fi
  if [ `echo $KEYWORD        | #
        sed 's/[^a-zA-Z]//g' | #
        wc -c` -lt 2 ]
   then
        echo "We need something to forget, please!"
        exit 
  fi

# --------------------------------------------------------------------------- #
#  D U M P   A L L   P A D S   I N T O   O N E (IF NECESSARY)
# --------------------------------------------------------------------------- #
  if [ -f $DUMPHERE ]; then
      #echo "DUMP does exist ($DUMPHERE)"
       read -p "UPDATE DUMP FROM REMOTE? [y/n] " ANSWER
       if [ "X$ANSWER" == "Xy" ];
             then rm $DUMPHERE; 
       elif [ "X$ANSWER" != "Xn" ];
             then  echo "You should answer with y or n!"; exit;
       fi
  fi;  if [ ! -f $DUMPHERE ]; then
       for PAD in `wget --no-check-certificate -q -O - $ALLPADNAMES | # 
                   egrep -v "graph_meandering|references.bib" | # IGNORE THIS
                   sed "s,^,$PADBASEURL/," | sed "s,$,.$XFORMAT," `
        do
            echo $PAD
            PADNAME=`basename $PAD`
            wget --no-check-certificate -q -O - $PAD | #
            sed "s,^,${PADNAME}:," >> $DUMPHERE
       done
  fi

# --------------------------------------------------------------------------- #
#  L O O K   F O R   K E Y W O R D 
# --------------------------------------------------------------------------- #

  if [ `grep -i "\b$KEYWORD\b" $DUMPHERE | # FIND KEY
        wc -l` -gt 0 ]; then

  cp $DUMPHERE ${DUMPHERE}.tmp

 ( IFS=$'\n'
  for LN in `grep -n "" ${DUMPHERE}.tmp | # NUMBER ALL LINES
             grep -i "\b$KEYWORD\b"     | # FIND KEY
             cut -d ":" -f 1`             # EXTRACT LINE NUMBER
   do
        PREVEMPTYLINE=`grep -n "" $DUMPHERE          | # NUMBER ALL LINES
                       head -n ${LN}                 | # PRINT FROM START TO LINE NUM
                       sed '/^[0-9]*:.*txt:[ \t]*$/s/^/X/' | # MARK EMPTY LINES
                       grep "^X"                     | # SELECT MARKED
                       tail -n 1                     | # SELECT LAST
                       sed 's/^X//'                  | # REMOVE MARK
                       cut -d ":" -f 1`                # EXTRACT LINE NUMBER
        NEXTEMPTYLINE=`grep -n "" $DUMPHERE          | # NUMBER ALL LINES
                       sed "1,${LN}d"                | # PRINT FROM LINE NUM TO END
                       sed '/^[0-9]*:.*txt:[ \t]*$/s/^/X/' | # MARK EMPTY LINES
                       grep "^X"                     | # SELECT MARKED
                       head -n 1                     | # SELECT FIRST
                       sed 's/^X//'                  | # REMOVE MARK
                       cut -d ":" -f 1`                # EXTRACT LINE NUMBER

        if [ X$PREVEMPTYLINE != X$LASTMATCH ]; then

      # MARK MATCHES (REMEMBER TO REMOVE)
        sed -i "${PREVEMPTYLINE},${NEXTEMPTYLINE}s/^/XXX /" ${DUMPHERE}.tmp

        fi

        LASTMATCH="$PREVEMPTYLINE"

  done )

# --------------------------------------------------------------------------- #
#  M O V E   I N   P L A C E
# --------------------------------------------------------------------------- #

       cat ${DUMPHERE}.tmp | grep -v "^XXX " > $DUMPHERE
       rm ${DUMPHERE}.tmp

  else
       echo "No $KEYWORD found."
  fi 



# --------------------------------------------------------------------------- #
#  C L E A N   U P   A N D   E X I T
# --------------------------------------------------------------------------- #


exit 0;
